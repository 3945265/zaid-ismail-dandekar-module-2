

// Defining class  
 class WinningApp {  
   var appName;  
   var category; 
   var developer; 
   var year;  
     
   // defining class function  
    showAppInfo() {  
        print("Name of app is : ${appName}");  
        print("Category of app is : ${category}");  
        print("Developer of app is : ${developer}"); 
        print("The year it won was : ${year}");  
  
               }
    toUpperCase() {
      print(appName.toUpperCase());
    }  
}  
void main () {  
  
  // Creating object called std  
  var app = new WinningApp();  
  app.appName = "Ambani Africa";  
  app.category = "Best Gaming Solution, Best Educational and Best South African Solution.";  
  app.developer = "Mukundi Lambani";
  app.year = 2021;
// Accessing class Function  
 app.showAppInfo();  
 app.toUpperCase();
}  
